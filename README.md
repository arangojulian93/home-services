# HomeServices

Home Service App v.1.5 
==========

Según las indicaciones dadas la aplicación fue planteado su funcionamiento y visualisacción de la siguiente manera:
Entras y te envia a una home, veras un menu con diferentes opciones para seleccionar
podras entrar desde estás opciones o desde el boton del menu de la home para crear una nueva factura, en la home el boton se llama "Añadir factura", en el menu aparece con el nombre "crear factura", una vez ingresas te aparece un formulario con campos a rellenar, el sistema automaticamente capta la ubicación de la persona, por tanto solo debes añadir los campos que te pide, seleccionas el usuario que pedira el servicio y seleccionas el servicio que deseas adquirir, una vez llenados los campos solo deberas enviar los datos, en esta versión el sistema guarda basandose en una regla de negocio en la cual  verifica el costo del servicio según la distancia en la que el usuario se encuentra y el tipo de categoria que tiene este, si el usuario está a mayor o menor distancia de cierta cantidad de kilometros según sú categoria le hace los calculos y guarda..
la regla de negocio aún no se a terminado, el sistema también actualiza, pero depende absolutamente de la terminación de esta regla, por ende el sistema solo guarda en base de datos más no actualiza aunque lo intentes, se lanza como una versión de prueba, como
muestra del avance y lo que se espera como resultado final. 


Resultados:
--------------------

Funciones:
+ Te envia a home.
+ Te muestra un mapa con tú ubicación.
+ Consume servicios y almacena información en base de datos mediante un formulario
+ verifica mediante regla de negocio el tipo de categoria que es el cliente y calcula 
con base en esto un costo por kilometro, según los kilometros admitidos para cada categoria de cliente
+ Actualiza más no guarda reutilizando el formulario.


Por Terminar:
+ falta validar sede más cercana del cliente, para que almacene los dos campos faltantes, "dirección", "namesede" y así actualice también en base de datos, siguiendo las normas planteadas en la documentación.
