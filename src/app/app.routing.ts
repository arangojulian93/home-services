import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Importar Componentes


import {HomeComponent}  from './components/home.component';
import {ErrorComponent} from './components/error-component';
import {AppMap} from './components/map-prueb.component';
import {FacturaAddComponent} from './components/factura-add.component';
import {FacturaEditComponent} from './components/factura-edit.component';



const appRoutes: Routes = [

   {path: '', component: HomeComponent},
   {path: 'home', component: HomeComponent},
   {path: 'crear-factura', component: FacturaAddComponent},
   {path: 'editar-factura/:id', component: FacturaEditComponent},
   {path: 'map', component:AppMap},
   {path: '**', component: ErrorComponent} // sí hay eror osea un 404 , cargueme empleado


];

export const appRoutingProviders: any[]= [];

export const routing: ModuleWithProviders= RouterModule.forRoot(appRoutes);