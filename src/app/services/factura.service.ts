import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import {Factura} from '../models/factura';
import {GLOBAL} from './global';


@Injectable() 

export class FacturaService {

	public url: string;

	constructor(
       
       public _http: HttpClient

    ){

		this.url = GLOBAL.url;
	}

	getFacturas():Observable<any>{
		//return "TEXTO DESDE EL SERVICIO";
		return this._http.get(this.url + 'facturas');
	}

	getFacturasid(id):Observable<any>{
		//return "TEXTO DESDE EL SERVICIO";
		return this._http.get(this.url + 'facturas');
	}

		addFactura(factura: Factura){
		let json= JSON.stringify(factura);
		let params = 'json='+json;
		let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');

		return this._http.post(this.url+'Facturas', params, {headers: headers});
    }

    editFactura(id, factura:Factura){
       let json= JSON.stringify(factura);
       let params = 'json='+json;
	   let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
     
     return this._http.post(this.url+'update-factura/'+id, params, {headers: headers});    
    }
}