import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import {Cliente} from '../models/cliente';
import {CLIENTES} from './clientes';


@Injectable() 

export class ClienteService {

	public url: string;

	constructor(
       
       public _http: HttpClient

    ){

		this.url = CLIENTES.url;
	}

	getClientes():Observable<any>{
		//return "TEXTO DESDE EL SERVICIO";
		return this._http.get(this.url + 'clientes');
	}
}