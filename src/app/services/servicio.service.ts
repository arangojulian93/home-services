import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import {Servicio} from '../models/servicio';
import {SERVICIOS} from './servicios';


@Injectable() 

export class ServicioService {

	public url: string;

	constructor(
       
       public _http: HttpClient

    ){

		this.url = SERVICIOS.url;
	}

	getServicios():Observable<any>{
		//return "TEXTO DESDE EL SERVICIO";
		return this._http.get(this.url + 'servicios');
	}
}