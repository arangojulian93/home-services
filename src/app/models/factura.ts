export class Factura{

	constructor(
        
          public id: number,
          public nombre: string,
          public direccion: string,
          public servicio: string,
          public nameSede: string,
          public costo: number
          
		){}
}