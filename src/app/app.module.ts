import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

//rutas
import {routing, appRoutingProviders} from './app.routing';

// Componentes 
import {HomeComponent} from './components/home.component';
import {AppMap} from './components/map-prueb.component';
import {FacturaAddComponent} from './components/factura-add.component';
import {ErrorComponent} from './components/error-component';
import {FacturaEditComponent} from './components/factura-edit.component';

import { AppComponent } from './app.component';

//servicios externos
//import {ClienteService} from './services/cliente.service';


// Importar HttpClientModule
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    AppMap,
    HomeComponent,
    FacturaAddComponent,
    FacturaEditComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpClientModule
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
