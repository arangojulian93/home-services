import { Component, OnInit  } from '@angular/core';

declare let L;

@Component({
  selector: 'map-prueb',
  templateUrl: '../views/map-prueb.html',
  styleUrls: ['../../assets/stylemap.css']
})
export class AppMap implements OnInit{


    constructor() {

    }

  title = 'Home-Services';

   ngOnInit() {
        const map = L.map('map').setView([-72.0000000, 4.00000000], 13);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
    }
}