import {Component} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {FacturaService} from '../services/factura.service';
import {Factura} from '../models/factura';
import {ClienteService} from '../services/cliente.service';
import {Cliente} from '../models/cliente';
import {ServicioService} from '../services/servicio.service';
import {Servicio} from '../models/servicio';

declare let L;

@Component({
	selector: 'factura-edit',
	templateUrl: '../views/factura-add.html',
	providers: [FacturaService,
                ClienteService,
                ServicioService
	           ],
    styleUrls: ['../../assets/stylemap.css']
	})

export class FacturaEditComponent{

	public title: string;
	public factura: Factura;
	public clientes: Cliente;
	public servicios: Servicio;

	constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _facturaService: FacturaService,
        private _clienteService: ClienteService,
        private _servicioService: ServicioService

		){
		this.title = 'Editar Factura';
		this.factura = new Factura(1,'','','','',0);
	}

	ngOnInit(){
		console.log(this.title);
		this.getlistClientes();
		this.getlistServicios();
		this.Infomapa();
		this.getFactura();	
	}

	onSubmit(){
         
         this.updateFactura();

	}


	getlistClientes(){

        //listado API clientes
		this._clienteService.getClientes().subscribe((clientes:Cliente)=>(this.clientes = clientes));

	}

	getlistServicios(){

        //listado API clientes
		this._servicioService.getServicios().subscribe((servicios:Servicio)=>(this.servicios = servicios));

	}

	Infomapa(){
		  const map = L.map('map').setView([6.156059, -75.58587729999999], 13);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        map.locate({enableHighAccuracy: true});
        map.on('locationfound', e =>{
        	    const coords = [e.latlng.lat, e.latlng.lng]
               const marker = L.marker(coords);
    			marker.bindPopup('You hare here!');
    			map.addLayer(marker);
    			
        	});


	}

	updateFactura(){
       this._route.params.forEach((params:Params)=>{
        let id = params['id'];
    	 this._facturaService.editFactura(id,this.factura).subscribe(
             response => {
                 
                 if(response['code'] == 200){
                    this._router.navigate(['/map']);
                 }else{
                    console.log(response);
                 }
             },

             error => {
                console.log(<any>error);
             }
            
            );
    	 });
	}

	getFactura(){
		this._route.params.forEach((params:Params)=>{
              let id = params['id'];

               this._facturaService.getFacturasid(id).subscribe(
             response => {
                 
                 if(response['code'] == 200){
                    this.factura = response.data;
                 }else{
                    this._router.navigate(['/map']);
                 }
             },

             error => {
                console.log(<any>error);
             }
            
            );

			});
	}
}