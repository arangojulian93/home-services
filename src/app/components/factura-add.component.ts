import {Component} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {FacturaService} from '../services/factura.service';
import {Factura} from '../models/factura';
import {ClienteService} from '../services/cliente.service';
import {Cliente} from '../models/cliente';
import {ServicioService} from '../services/servicio.service';
import {Servicio} from '../models/servicio';

declare let L;
var coords;


@Component({
	selector: 'facturas-add',
	templateUrl: '../views/factura-add.html',
	providers: [FacturaService,
                ClienteService,
                ServicioService
	           ],
    styleUrls: ['../../assets/stylemap.css']
	})

export class FacturaAddComponent{
	public title: string;
	public clientes: Cliente;
	public facturas:any;
	public factura: Factura;
	public servicios: Servicio;



	constructor(
            
        private _route: ActivatedRoute,
        private _router: Router,
        private _facturaService: FacturaService,
        private _clienteService: ClienteService,
        private _servicioService: ServicioService

		){
		this.title = 'Webapp de Facturas';
		this.factura = new Factura(0,'','','','',0);
	}
	ngOnInit(){
		console.log('se a cargado el componente factura.component.ts');
		//alert(this._facturaService.getFacturas());

         this.getlistServicios();
         this.getlistClientes();

         this.getlistFacturas();

         this.Infomapa();
		
	
	}
    
    onSubmit(){

           
           this.Regla();
          
         this.Facturadd();
         
    }
     
	 getlistClientes(){

        //listado API clientes
		this._clienteService.getClientes().subscribe((clientes:Cliente)=>(this.clientes = clientes));

	}

	getlistServicios(){

        //listado API clientes
		this._servicioService.getServicios().subscribe((servicios:Servicio)=>(this.servicios = servicios));

	}

	Infomapa(){
		  const map = L.map('map').setView([6.156059, -75.58587729999999], 13);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        map.locate({enableHighAccuracy: true});
        map.on('locationfound', e =>{
        	     coords = [e.latlng.lat, e.latlng.lng]
               const marker = L.marker(coords);
    			marker.bindPopup('You hare here!');
    			map.addLayer(marker);
    			

        	});


	}

	getlistFacturas(){

			//listado Facturas
		this._facturaService.getFacturas().subscribe(
              
            result => {
              
              this.facturas =  result.data;

              if(result.code != 200){
                   
                   console.log(result);

              }else{

                this.facturas = result.data;

              }

            },

            error=> {
                
                console.log(<any>error);
            
            });


	}

  Facturadd(){

        console.log(this.factura, this.servicios[0] );

       this._facturaService.addFactura(this.factura).subscribe(
             response => {
                 
                 if(response['code'] == 200){
                    this._router.navigate(['/map']);
                 }else{
                    console.log(response);
                 }
             },

             error => {
                console.log(<any>error);
             }
            
           );
  }


 Dist(lat1, lon1, lat2, lon2) {
    var rad = function (x) {
         return x * Math.PI / 180;
     }
 
     var R = 6378.137;//Radio de la tierra en km
     var dLat = rad(lat2 - lat1);
     var dLong = rad(lon2 - lon1);
     var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
     var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
     var d = R * c;
     return d.toFixed(3);//Retorna tres decimales
}

  Regla(){

   for (var i in this.clientes) { 
   

     if(this.factura.nombre == this.clientes[i].name){
          
        for(var n in this.servicios){
            
          if(this.factura.servicio == this.servicios[n].name){

            for(var a in this.servicios[n].locations){
                
              if(this.servicios[n].locations[a]){

             var res =   this.Dist(this.servicios[n].locations[a].lat,this.servicios[n].locations[a].lng,coords[0],coords[1]);
                

                console.log(this.servicios[n].locations[a], res);
          
               
              switch (this.clientes[i].category) {
                case 'GOLD':
                  var tarifabase = 1000;
                  var km=8;
                  var vkmenor= 90;
                  var vkmayor= 81; 
                  var precio = this.servicios[n].price; 
                   
                   var numFinal = parseFloat(res);
                   
                   if(numFinal<km ){
                     var d = numFinal*vkmenor;
                       this.factura.costo = precio+tarifabase+d;
                     console.log(this.factura.costo);
                    }else if(numFinal>=km){
                      var d = numFinal*vkmayor;
                       this.factura.costo = precio+tarifabase+d;
                     console.log(this.factura.costo);
                         
                    }
                  
                  
                    
                    console.log(this.servicios[n], this.factura.costo, numFinal);

                  break;
                case 'SILVER':
                  var tarifabase = 2000;
                  var km=10;
                  var vkmenor= 100;
                  var vkmayor= 90; 
                  var precio = this.servicios[n].price; 

                  var numFinal = parseFloat(res);
                   
                   if(numFinal<km ){
                     var d = numFinal*vkmenor;
                       this.factura.costo = precio+tarifabase+d;
                     console.log(this.factura.costo);
                    }else if(numFinal>=km){
                      var d = numFinal*vkmayor;
                       this.factura.costo = precio+tarifabase+d;
                     console.log(this.factura.costo);
                         
                    }
                    
                    console.log(this.servicios[n]);

                  break;
                case 'PLATINUM':
                  var tarifabase = 500;
                  var km=5;
                  var vkmenor= 80;
                  var vkmayor= 50; 
                  var precio = this.servicios[n].price; 
                    
                         var numFinal = parseFloat(res);
                   
                   if(numFinal<km ){
                     var d = numFinal*vkmenor;
                       this.factura.costo = precio+tarifabase+d;
                     console.log(this.factura.costo);
                    }else if(numFinal>=km){
                      var d = numFinal*vkmayor;
                       this.factura.costo = precio+tarifabase+d;
                     console.log(this.factura.costo);
                         
                    }
                    console.log(this.servicios[n]);

                  break;
   
                default:

                  console.log(this.clientes[i].category);
                  //Sentencias_def ejecutadas cuando no ocurre una coincidencia con los anteriores casos
                  break;
              }

                }
            }

              console.log(this.factura.servicio);

            }
          }
        console.log(this.factura.nombre);
      }

    }

  }
}