import {Component} from '@angular/core';


@Component({
	selector: 'error',
	templateUrl: '../views/error.html'
	})

export class ErrorComponent{
	public title: string;

	constructor(){
		this.title = 'Error!! Página no encontrada.';
	}

	ngOnInit(){
		console.log('se a cargado el componente Error.component.ts');
	}

}