import {Component} from '@angular/core';


@Component({
	selector: 'home',
	templateUrl: '../views/home.html'
	})

export class HomeComponent{
	public title: string;

	constructor(){
		this.title = 'Webapp de Facturas con Angular7';
	}

	ngOnInit(){
		console.log('se a cargado el componente home.component.ts');
	}

}